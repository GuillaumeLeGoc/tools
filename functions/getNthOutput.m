function outn = getNthOutput(FUN, n, varargin)
% outn = getNthOutput(FUN, n, varargin)
%
%	Provides the nth output from the function FUN. Support up to 5.
%
% INPUTS :
% ------
% FUN : function handle
% n : n th output argument required
% all the rest is passed to the function

% --- Check input
p = inputParser;
p.addRequired('FUN', @(x) isa(x, 'function_handle'));
p.addRequired('n', @isscalar);
p.parse(FUN, n);

% --- Processing
if n == 1
    outn = FUN(varargin{:});
elseif n == 2
    [~, outn] = FUN(varargin{:});
elseif n == 3
    [~, ~, outn] = FUN(varargin{:});
elseif n == 4
    [~, ~, ~, outn] = FUN(varargin{:});
elseif n == 5
    [~, ~, ~, ~, outn] = FUN(varargin{:});
else
    error('Number of arguments unsupported, please edit the file and add your ~ !');
end

end
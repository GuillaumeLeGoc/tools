function colors = getColors(n_colors)
% Retrieve RGB vectors corresponding to default MATLAB plot colors.
% Was custom written before the discovery of the LINES function that does
% the same thing but better. This function is maintained for compatibility.

if exist('n_colors', 'var')
    colors = lines(n_colors);
else
    colors = lines;
end

end
function traj(this, traj, varargin)
%G_CODE::TRAJ Trajectory element
%*  TRAJ(LIST) where LIST is a n-by-6 array [x y z x0 y0 OR] add a trajectory passing
%   by all the points (x,y,z). R is the in-xy-plane radius of curvature.
%   Linear segments have R=Inf, clockwise arcs have a finite R<-d/2 and 
%   counterclockwise arc have a finite R>d/2, where d is the distance between
%   point n-1 and point n. R(1) is always ignored.
%
%*  TRAJ(..., 'key', value) specifies options. Keys can be:
%   * 'tool' (this.tool):   The tool to use.
%   * 'step_size' (tool.diam): The step size.
%
%*  See also: G_CODE, G_CODE::hole.

% === Input variables =====================================================

in = inputParser;
in.addRequired('traj', @isnumeric);
in.addParamValue('tool', this.tool, @isnumeric);
in.addParamValue('z_step', this.tools(this.tool).diam, @isnumeric);
in.addParamValue('z_start', 0, @isnumeric);

in.parse(traj, varargin{:});
in = in.Results;

% =========================================================================

% --- Checks

% Check tool
this.check();

if ~numel(in.traj)
    return;
end

if size(in.traj,2)==3
    in.traj = [in.traj NaN(size(in.traj,1), 3)];
end

% --- Add element
this.elms{end+1} = struct('type', 'traj', ...
                          'tool', in.tool, ...
                          'traj', in.traj, ...
                          'z_step', in.z_step, ...
                          'z_start', in.z_start);

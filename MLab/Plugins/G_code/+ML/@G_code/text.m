function text(this, txt, p, varargin)
%G_CODE::TEXT Generate dotted text
%*  TEXT(TXT, POS, Z) where POS is a 1-by-2 array (x,y) and Z is the depth
%   to drill writes TXT at POS with depth Z.
%
%*  TEXT(..., 'width', W) specifies the width W of the text.
%
%*  TEXT(..., 'height', H) specifies the height H of the text.
%
%*  TEXT(..., 'tool', T) specifies the tool to use.
%
%*  See also: G_code, G_code::drill.

% === Input variables =====================================================

in = inputParser;
in.addRequired('txt', @ischar);
in.addRequired('p', @isnumeric);
in.addParamValue('z', -this.tools(this.tool).diam/2, @isnumeric);
in.addParamValue('width', NaN, @isnumeric);
in.addParamValue('height', NaN, @isnumeric);
in.addParamValue('scale', 1, @isnumeric);
in.addParamValue('tool', this.tool, @isnumeric);

in.parse(txt, p, varargin{:});
in = in.Results;

% =========================================================================

% Get the text pattern
[T, M] =  Patterns.Fonts.dotfont(in.txt);

% Get the positions of the holes
[J I] = find(M);

if ~isnan(in.width)
    dx = (in.width-this.tools(this.tool).diam)/(max(I)-1);
else
    dx = this.tools(this.tool).diam*1.5*in.scale;
end

if ~isnan(in.height)
    dy = (in.height-this.tools(this.tool).diam)/(max(J)-1);
else
    dy = this.tools(this.tool).diam*1.5*in.scale;
end

pos = ones(numel(I),1)*(in.p+this.tools(this.tool).diam/2) + ...
      [(I-1)*dx (max(J)-J)*dy];

% Create the holes
this.hole(pos, in.z, 'tool', in.tool);


function [h1, h2] = visu_substrate(this)
%G_CODE::VISU_SUBSTRATE Display the substrate
%*  VISU_SUBSTRATE() display the substrate.
%
%*  See also: G_CODE.


hold on

box on
daspect([1 1 1]);

x0 = 0;
x1 = this.substrate.dims(1);
y0 = 0;
y1 = this.substrate.dims(2);

h1 = line([ x0 x1 x1 x0 ; x1 x1 x0 x0], [y0 y0 y1 y1 ; y0 y1 y1 y0], 'color', 'k');

X0 = Inf;  Y0 = Inf;
X1 = -Inf; Y1 = -Inf;

for i = 1:numel(this.tools)
 
    xm = min(this.trajs(i).traj(:,1))-this.tools(i).diam/2;
    xM = max(this.trajs(i).traj(:,1))+this.tools(i).diam/2;
    ym = min(this.trajs(i).traj(:,2))-this.tools(i).diam/2;
    yM = max(this.trajs(i).traj(:,2))+this.tools(i).diam/2;
    
    if xm<X0, X0 = xm; end
    if xM>X1, X1 = xM; end
    if ym<Y0, Y0 = ym; end
    if yM>Y1, Y1 = yM; end
    
end

h1 = line([ X0 X1 X1 X0 ; X1 X1 X0 X0], [Y0 Y0 Y1 Y1 ; Y0 Y1 Y1 Y0], ...
          'color', 'k', 'LineStyle', '--');

a = [min(x0, X0) max(x1, X1) min(y0, Y0) max(y1, Y1)];
dx = a(2)-a(1);
dy = a(4)-a(3);
a = a + [-dx dx -dy dy]*0.1;
axis(a);
function visu_traj(this, tools)
%G_CODE::VISU_TRAJ Display the trajectories
%*  VISU_TRAJ() display the trajectories for the different tools.
%
%*  See also: G_CODE.

% --- Default values
if ~exist('tools', 'var')
    tools = 1:numel(this.tools); 
end

% --- Generate trajectories (optional)
for i = tools
    if numel(this.trajs)<i || isempty(this.trajs(i).traj)
        this.generate(i);
    end
end

% --- Preparation
hold on
cm = lines(numel(tools));

% --- Display trajectories
for i = 1:numel(tools)

    tool = tools(i);
    
    x = [];
    y = [];
    z = [];
    for j = 1:size(this.trajs(tool).traj,1)
        
        if any(isnan(this.trajs(tool).traj(j,1:3)))
            continue;
        elseif isnan(this.trajs(tool).traj(j,end))
            x(end+1) = this.trajs(tool).traj(j,1);
            y(end+1) = this.trajs(tool).traj(j,2);
            z(end+1) = this.trajs(tool).traj(j,3);
        else
            A = this.trajs(tool).traj(j-1,1:2);
            B = this.trajs(tool).traj(j,1:2);
            C = this.trajs(tool).traj(j,5:6);
            R = sqrt(sum((A-C).^2));
            
            if all(A==B)
                T1 = ML.G_code.vec2ang(A-C);
                T = T1:pi/180:T1+2*pi;
            else
                
                if this.trajs(tool).traj(j,7)>0
                    T1 = ML.G_code.vec2ang(A-C);
                    T2 = ML.G_code.vec2ang(B-C);
                    if T2<T1, T2 = T2+2*pi; end
                    T = linspace(T1, T2, round(abs(T2-T1)*180/pi));
                else
                    T1 = ML.G_code.vec2ang(B-C);
                    T2 = ML.G_code.vec2ang(A-C);
                    if T2<T1, T2 = T2+2*pi; end
                    T = fliplr(linspace(T1, T2, round(abs(T2-T1)*180/pi)));
                end
                   
            end
            
            x = [x C(1)+R*cos(T)];
            y = [y C(2)+R*sin(T)];
            z = [z this.trajs(tool).traj(j,3)*ones(1,numel(T))];
        end
    end
        
    plot3(x, y, z, '.-', 'color', cm(i,:));
    
end

% --- Adjustment
box on
axis square
daspect([1 1 1]);


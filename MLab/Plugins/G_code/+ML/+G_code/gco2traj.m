function traj = gco2traj(fname)
%GCO2TRAJ G_code to trajectory

fid = fopen(fname);
tline = '';
pos = NaN(1,3);
traj = NaN(0,3);

while true
    
    line = fgetl(fid);
    if ~ischar(line), break; end
    
    % --- Remove comments
    line = regexprep(line, '\(.*\)', '');
    
    % --- Remove empty lines
    line = strtrim(line);
    if isempty(line), continue; end
    
    % disp(line)
    
    % --- Find X, Y, Z commands
    [~, ~, ~, X] = regexp(line, 'X\-?\d*(\.\d*)?');
    [~, ~, ~, Y] = regexp(line, 'Y\-?\d*(\.\d*)?');
    [~, ~, ~, Z] = regexp(line, 'Z\-?\d*(\.\d*)?');
        
    % --- Interpret commands
    if (~isempty(X)), pos(1) = str2double(X{1}(2:end)); end
    if (~isempty(Y)), pos(2) = str2double(Y{1}(2:end)); end
    if (~isempty(Z)), pos(3) = str2double(Z{1}(2:end)); end

    if ~any(isnan(pos))
        if size(traj,1)==0 || any(traj(end,:)~=pos)
            traj(end+1,:) = pos;
        end  
    end
    
end
fclose(fid);
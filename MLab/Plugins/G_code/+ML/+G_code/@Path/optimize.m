function I = optimize(this)
%GEOM.PATH.OPTIMIZE_PATH optimize a 2D path
%
%*  See also: Geom.Path.

% --- Direct output
if this.N<4
    I = 1:this.N;
    return
end

% --- Optimization
slx = linspace(min(this.pos(:,1)), max(this.pos(:,1)), ceil(sqrt(this.N/2)));
sly = linspace(min(this.pos(:,2)), max(this.pos(:,2)), ceil(sqrt(this.N/2)));

% --- Path in x
Ix = [];
for i = 1:numel(slx)-1
    if i==1
        J = find(this.pos(:,1)>=slx(i) & this.pos(:,1)<=slx(i+1));
    else
        J = find(this.pos(:,1)>slx(i) & this.pos(:,1)<=slx(i+1));
    end
    tmp = sortrows([this.pos(J,2) (1:numel(J))'], mod(i,2)*2-1);
    Ix = [Ix ; J(tmp(:,2))];
end

% --- Path in y
Iy = [];
for i = 1:numel(sly)-1
    if i==1
        J = find(this.pos(:,2)>=sly(i) & this.pos(:,2)<=sly(i+1));
    else
        J = find(this.pos(:,2)>sly(i) & this.pos(:,2)<=sly(i+1));
    end
    tmp = sortrows([this.pos(J,1) (1:numel(J))'], mod(i,2)*2-1);
    Iy = [Iy ; J(tmp(:,2))];
end

% --- Find the best path
dx = sum(sqrt(sum(diff(this.pos(Ix,:)).^2,2)));
dy = sum(sqrt(sum(diff(this.pos(Iy,:)).^2,2)));

% --- Output
if dx<=dy
    I = Ix;
else
    I = Iy; 
end

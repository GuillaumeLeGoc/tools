function a = vec2ang(v, type)
%GEOM.VEC2ANG Get vectors' angles
%*  A = GEOM.VEC2ANG(V) get the angles (in radians) of the vectors V. 
%   V should be a N-by-2 array.
%
%[  Note: The output angle lie in [0, 2*pi[.  ]
%
%*  GEOM.VEC2ANG(V, 'deg') returns degrees instead of radians.
%
%*  See also: angle.

% Get the angles
a = mod(angle(v(:,1) + sqrt(-1)*v(:,2)), 2*pi);

% Degrees
if nargin==2 && strcmp(type, 'deg')
    a = a*180/pi;
end

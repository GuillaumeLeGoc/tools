function gco2def(fname, varargin)
%GCO2DEF G-code to definiton

% === Input variables =====================================================

in = inputParser;
in.addRequired('fname', @ischar);
in.addParamValue('mode', 'xy', @ischar);

in.parse(fname, varargin{:});
in = in.Results;

% =========================================================================

Patterns.traj2def(Patterns.gco2traj(in.fname), 'mode', in.mode);


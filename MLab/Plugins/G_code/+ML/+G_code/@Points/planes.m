function P = planes(this, I)
%GEOM.POINTS.PLANES get planes coefficients 
%*  P = GEOM.POINTS.PLANES(I) get the coefficients of planes.
%   The triplets of points are specified by I, a M-by-3 array of index.
%   The output coefficients A, B, C and D are vectors of length M, defined 
%   by the Cartesian equation:
%       Ax + By + Cz + D = 0
% 
%*  See also: Geom.Points, Geom.Points.lines

% Default index
if ~exist('I', 'var'), I = [1 2 3]; end

% Computation
x = [this.pos(I(:,1),1) this.pos(I(:,2),1) this.pos(I(:,3),1)];
y = [this.pos(I(:,1),2) this.pos(I(:,2),2) this.pos(I(:,3),2)];
z = [this.pos(I(:,1),3) this.pos(I(:,2),3) this.pos(I(:,3),3)];

A = y(:,1).*(z(:,2)-z(:,3)) + y(:,2).*(z(:,3)-z(:,1)) + y(:,3).*(z(:,1)-z(:,2));
B = z(:,1).*(x(:,2)-x(:,3)) + z(:,2).*(x(:,3)-x(:,1)) + z(:,3).*(x(:,1)-x(:,2));
C = x(:,1).*(y(:,2)-y(:,3)) + x(:,2).*(y(:,3)-y(:,1)) + x(:,3).*(y(:,1)-y(:,2));

% Normalization
n = sqrt(A.^2+B.^2+C.^2);
A = A./n;
B = B./n;
C = C./n;

% Upperization
I = C<0;
if nnz(I)
    A(I) = -A(I);
    B(I) = -B(I);
    C(I) = -C(I);
end
D = -A.*x(:,1) - B.*y(:,1) - C.*z(:,1);

P = struct('form', 'Cartesian', 'A', A, 'B', B, 'C', C, 'D', D);
function disp(this, varargin)
%ML.Rect.disp
%   [ML.RECT].DISP() display the rectangle.
%
%   See also ML.Rect.

rectangle('position', this.xywh, varargin{:});
function out = grey(in)
%ML.color
%
%   See also: ML.

out = repmat(in, [1,3]);
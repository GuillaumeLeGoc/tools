function out = line(val, axis, varargin)
%ML.Visu.line Draw a line spanning the plotting box.
%   ML.VISU.LINE(VAL, AXIS) add a line on the current plot at values in
%   the numerical array VAL. AXIS is a string with possible values 'x' or 
%   'y'.
%
%   ML.VISU.LINE(..., 'Propertyname', PROPERTY) add properties which are
%   the same than for the plot function.
%
%   H = ML.VISU.LINE(...) returns a cell of handles.
%
%   See also line.

%!  TODO: Improve code.

% === Input variables =====================================================

in = inputParser;
in.KeepUnmatched = true;
in.addRequired('val', @isnumeric);
in.addRequired('axis', @(x) regexp(x,'[xy]'));

in.parse(val, axis, varargin{:});
Sin = in.Unmatched;
in = in.Results;

% =========================================================================

N = numel(in.val);
F = fields(Sin);

% --- Define properties
P = cell(N, 2*numel(F));

for i = 1:numel(F)
    
    if iscell(Sin.(F{i}))
        
        % Check number of elements
        if numel((Sin.(F{i})))==N
            for j = 1:N
                P{j, 2*i-1} = F{i};
% % %                 if strcmpi(F{i}, 'color')
% % %                     P{j, 2*i} = Color(Sin.(F{i}){j});
% % %                 else
                    P{j, 2*i} = Sin.(F{i}){j};
% % %                 end
            end
        else
            error('VISU.LINE:input', ['VISU.LINE: Wrong number of values for the property ' F{i} '. ' num2str(N) ' expected and ' num2str(numel(Sin.(F{i}))) ' received. Aborting.']);
        end
        
    else
        for j = 1:N
            P{j, 2*i-1} = F{i};
% % %             if strcmpi(F{i}, 'color')
% % %                 P{j, 2*i} = Color(Sin.(F{i}));
% % %             else
                P{j, 2*i} = Sin.(F{i});
% % %             end
        end
    end
end

% --- Display
h = cell(N, 1);
switch in.axis
    
    case 'x'
        lim = get(gca, 'Ylim');
        for i = 1:N
            h{i} = line([in.val(i) in.val(i)], [lim(1) lim(2)], P{i,:});
        end
        set(gca, 'Ylim', lim);
        
    case 'y'
        lim = get(gca, 'Xlim');
        for i = 1:N
            h{i} = line([lim(1) lim(2)], [in.val(i) in.val(i)], P{i,:});
        end
        set(gca, 'Xlim', lim);
end

% --- Output
if nargout
    out = h; 
end
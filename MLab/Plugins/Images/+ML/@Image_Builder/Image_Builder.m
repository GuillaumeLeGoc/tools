classdef Image_Builder<handle
%ML.Image_Builder is the Image Builder class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties (SetAccess = public, GetAccess = public)
        
        width = NaN;
        height = NaN;
        background_color = [1 1 1];
        transparency_color = NaN;
        
        Blocks = struct('x', {}, 'y', {}, 'content', {});
        
    end
    
    properties (SetAccess = private, GetAccess = public)
    
        Img = ML.Image();
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Image_Builder()
        %ML.Image_Builder constructor
                    
        end
        
    end
end

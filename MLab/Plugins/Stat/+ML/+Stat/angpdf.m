function p = angpdf(bin, val, varargin)
%STAT.ANGPDF Angular probability distribution function
%*  P = STAT.ANGPDF(BINS, VALUES) computes the angular pdf of the vector 
%   VALUES, over the  bins edges BINS. BINS can be either a n-by-1 binning 
%   vector or a single value representing the number of bins. It returns a 
%   structure P  containing the following fields: 'bin', 'hist', 'pdf'.
%
%*  See also: Stat.pdf.

% === Input variables =====================================================

in = inputParser;
in.addRequired('bin', @isnumeric);
in.addRequired('val', @isnumeric);

in.parse(bin, val, varargin{:});
in = in.Results;

% =========================================================================

h = histc(t,bins);
h(end-1) = h(end-1) + h(end);
h = h/sum(h(1:end-1))/diff(bins(1:2));

hold on
prev = [NaN NaN];
for i = 1:numel(h)-1
    
    XY = circarc([0 0],h(i),bins(i),bins(i+1),60,0);
    
    plot(XY(:,1),XY(:,2),'-','color',color,'Linewidth',2);
    if isnan(prev(1))
        init = XY(1,:);
    else
        line([XY(1,1) prev(1)],[XY(1,2) prev(2)],'color',color);
    end
    prev = XY(end,:);
end
line([init(1) prev(1)],[init(2) prev(2)],'color',color);

XY = circarc([0 0],rv,lim(1),lim(2),360,0);
plot(XY(:,1),XY(:,2),'k--');

axis equal

if nargout
    p = struct('bin',bins,'pdf',h);
end
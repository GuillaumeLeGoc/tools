function P = kde(bin, val, varargin)
%STAT.KDE Kernel Density Estimator of probability distribution functions.
%*  P = STAT.KDE(BIN, VALUES, 'kernel', KERNEL, 'param', PARAM) return the
%   Kernel Density Estimation of the set VALUES over BINS. PARAM is an
%   array of parameters corresponding to the choosen kernel. KERNEL is a 
%   string whose value can be:
%
%   * 'Gaussian' (default): a Gaussian kernel. PARAM should be [sigma] 
%       where sigma is the Gaussian standard deviation.
%
%   * 'Square': a square step of length L. PARAM shoud be [L].
%
%   The output P is a struct with fields 'bin, 'hist' and 'pdf'.
%
%*  See also: Stat.pdf.

% === Input variables =====================================================

in = inputParser;
in.addRequired('bin', @isnumeric);
in.addRequired('val', @isnumeric);
in.addParamValue('kernel', 'Gaussian', @ischar);
in.addParamValue('param', [], @isnumeric);

in.parse(bin, val, varargin{:});
in = in.Results;

% =========================================================================

% --- Prepare output
P = struct('bin', in.bin, ...
           'hist', zeros(1, numel(in.bin)), ...
           'pdf', []);
       
switch lower(in.kernel)
    
    case 'square'
        
        % --- Get parameters
        if numel(in.param)<1
            warning(['[STAT.KDE] Wrong number of parameters: 1 expected and ' num2str(numel(in.param)) ' given.']);
            return
        end
        
        L = in.param(1);
        
        % --- Computation
        try 
            [B, V] = meshgrid(in.bin, in.val);
            P.hist = sum(abs(B-V)<=L/2,1);
        catch
            for i = 1:numel(in.val)
                P.hist = P.hist + double(abs(in.bin-in.val(i))<=L/2);
                if ~mod(i,numel(in.val)/100)
                    Time.status
                end
            end
        end
            
    case 'gaussian'
        
        % --- Get parameters
        if numel(in.param)<1
            warning(['[STAT.KDE] Wrong number of parameters: 1 expected and ' num2str(numel(in.param)) ' given.']);
            return
        end
        
        sigma = in.param(1);
        
        % --- Computation
        try 
            [B, V] = meshgrid(in.bin, in.val);
            P.hist = sum(exp(-((B-V).^2)/2/sigma^2),1);
        catch
            for i = 1:numel(in.val)
                P.hist = P.hist + exp(-((in.bin-in.val(i)).^2)/2/sigma^2);
                if ~mod(i,numel(in.val)/100)
                    Time.status
                end
            end
        end
end

% --- Normalization
P.pdf = P.hist/trapz(in.bin, P.hist);



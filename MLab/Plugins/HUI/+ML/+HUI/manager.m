function out = manager(varargin)
%ML.HUI.manager HUI manager
%
%   See also: ML.HUI.

% === Persistent variables ================================================

persistent HUI

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('set', NaN, @(x) isa(x, 'ML.HUI'));
in.addParamValue('get', '', @ischar);
in = +in;

% =========================================================================

% --- Instantiation
if isnumeric(HUI)
    HUI = {};
end

% --- Declaration
if isa(in.set, 'ML.HUI')
    HUI.(in.set.name) = in.set;
end

% --- Get
if ~isempty(in.get) && nargout
    out = HUI.(in.get);
end
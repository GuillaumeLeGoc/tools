function tag = input(this, varargin)
%ML.HUI.input HUI input field
%   ELM = ML.HUI.INPUT(ID) Creates a HUI text input identified by ID. ELM 
%   is a html string which can be used as an input in ML.HUI.cell.
%
%   ML.HUI.INPUT(..., 'text', TEXT) adds some default text.
%
%   ML.HUI.INPUT(..., EVENT, CALLBACK) adds a callback related to an 
%   event. Possible values for EVENT are:
%   - 'change'
%   - 'click'
%   - 'mouseover'
%   - 'mouseout'
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.text = @ischar;
in.tab(this.current_tab) = @isnumeric;
in.tag('') = @ischar;
in.label('') = @ischar;
in.style('') = @ischar;

in.data(struct()) = @isstruct;
in.click('') = @ML.isfunction_handle;
in.mouseover('') = @ML.isfunction_handle;
in.mouseout('') = @ML.isfunction_handle;
in.change('') = @ML.isfunction_handle;
in.visible(true) = @islogical;
in.disabled(false) = @islogical;

in = +in;

% =========================================================================

% --- Define widget
in.type = 'input';
in.tag = this.def_widget(in);

% --- Output
if nargout
    tag = in.tag;
end
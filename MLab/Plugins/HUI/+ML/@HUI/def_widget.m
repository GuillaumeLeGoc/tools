function tag = def_widget(this, varargin)
%ML.HUI.def_widget
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;
in.W = @isstruct;
in = +in;

% =========================================================================

% --- Manage tag
if isempty(in.W.tag)
    in.W.tag = ML.uniqid;
end

% --- Define the widget
this.tabs(in.W.tab).widgets{end+1} = in.W;

% --- Layout type peculiarities
if this.grid_layout 
    
    % Extend option grid ?
    if any(size(this.tabs(in.W.tab).layout) < [max(in.W.row) max(in.W.col)])
        this.tabs(in.W.tab).layout(max(in.W.row), max(in.W.col)).options = [];
    end

else
    
    % Insert tag in the html
    this.html.text(in.W.pos, ['<[' in.W.tag ']>']);
    
end

% --- Output
tag = in.W.tag;
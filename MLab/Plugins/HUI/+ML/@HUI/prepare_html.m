function prepare_html(this)
%ML.HUI.set_header
%
%   See also: ML.HUI.

% --- Definitions
nl = char([10 9]);

% --- Purge html
this.html = ML.HTML;

% --- Define the title
this.html.title(this.name);

% --- Get the style
config = ML.Config.get;
this.html.css([config.path 'Plugins/HUI/Styles/' this.style '.css']);

% --- Tabs display
if ~numel(this.tabs)==1 || ~isempty(this.tabs(1).name)
    tabs = this.html.div('id', 'tabs');
    for i = 1:numel(this.tabs)
        
        options = {};
        if i==this.display_tab
            options{end+1} = 'class';
            options{end+1} = 'tab_selected';
        end
        
        href = ['matlab:ML.HUI.tabs(''' this.name ''', ''show_tab'', ' num2str(i) ');'];
        this.html.a(tabs, href, this.tabs(i).name, options{:});
    end
end

% --- Callback script
script = ['function callback(Ctag, Cback) { ' nl ...
    'return "matlab:ML.HUI.callback(''' this.name ''', ' num2str(this.display_tab) ', ''"+Ctag+"'', ''"+Cback+"'');";' nl ...
    '}' nl nl ...
    'function update(Utag, Key, Value) { ' nl ...
    'return "matlab:ML.HUI.update(''' this.name ''', ' num2str(this.display_tab) ',''"+Utag+"'', ''"+Key+"'', ''"+Value+"'');";' nl ...
    '}' nl nl ...
    'function upback(Utag, Key, Value, Ctag, Cback) { ' nl ...
    'return "matlab:ML.HUI.update(''' this.name ''', ' num2str(this.display_tab) ',''"+Utag+"'', ''"+Key+"'', ''"+Value+"''); ML.HUI.callback(''' this.name ''', ' num2str(this.display_tab) ', ''"+Ctag+"'', ''"+Cback+"'');";' nl ...
    '}'];

this.html.text(this.html.script(this.html.head), script);

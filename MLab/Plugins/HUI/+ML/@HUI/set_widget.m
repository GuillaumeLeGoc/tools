function set_widget(this, varargin)
%ML.HUI.get_widget
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

in.src = @(x) isstruct(x) || ischar(x);

in = +in;


% =========================================================================

% --- Default tab
if ischar(in.src)
    in.src = struct('tab', this.display_tab, 'tag', in.src);
end

% --- Set widget
I = cellfun(@(x) strcmp(x.tag, in.src.tag), this.tabs(in.src.tab).widgets);

if isstruct(varargin{2})    
    this.tabs(in.src.tab).widgets{I} = varargin{2};
elseif ischar(varargin{2})
    
    if strcmp(varargin{3}, 'true'), varargin{3} = true; end
    if strcmp(varargin{3}, 'false'), varargin{3} = false; end
    
    this.tabs(in.src.tab).widgets{I}.(varargin{2}) = varargin{3};
end
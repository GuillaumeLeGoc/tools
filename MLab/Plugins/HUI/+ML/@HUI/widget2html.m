function out = widget2html(this, varargin)
%ML.HUI.widget2html
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;
in.W = @(x) ischar(x) || isstruct(x);
in = +in;

% =========================================================================

% --- Get widget
if ischar(in.W)
    in.W = this.get_widget(in.W);
end

% --- Prepare output
out = '';

% --- Check visibility
if ~in.W.visible, return; end

% --- Build html output
switch in.W.type
    
    case 'text'
        
        out = ['<span id="' in.W.tag '"'];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        
        out = [out '>' in.W.text '</span>'];
        
    case 'link'
        
        out = ['<a href="" id="' in.W.tag '"' opt_actions(in.W)];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        
        out = [out '>' in.W.text '</a>'];
        
    case 'button'
        
        out = ['<button type="button" id="' in.W.tag '"' opt_actions(in.W)];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out '>' in.W.text '</button>'];
        
    case 'input'
        
        if ~isempty(in.W.label)
            out = [out '<label for="' in.W.tag '">' in.W.label '</label>'];
        end
        
        out = [out '<input type="text" id="' in.W.tag '"' opt_actions(in.W) ' value="' in.W.text ...
            '" oninput="document.location=update(''' in.W.tag ''', ''text'', this.value);"'];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out '>'];
        
    case 'password'
        
        if ~isempty(in.W.label)
            out = [out '<label for="' in.W.tag '">' in.W.label '</label>'];
        end
        
        out = [out '<input type="password" id="' in.W.tag '"' opt_actions(in.W) ' value="' in.W.text ...
            '" oninput="document.location=update(''' in.W.tag ''', ''text'', this.value);"'];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out '>'];
        
    case 'textarea'
        
        if ~isempty(in.W.label)
            out = [out '<label for="' in.W.tag '">' in.W.label '</label>'];
        end
        
        out = [out '<textarea id="' in.W.tag '"'];
        
        if ~isnan(in.W.rows), out = [out ' rows="' num2str(in.W.rows) '"']; end
        if ~isnan(in.W.cols), out = [out ' cols="' num2str(in.W.cols) '"']; end
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out opt_actions(in.W) ' oninput="document.location=update(''' ...
            in.W.tag ''', ''text'', this.value);">'  in.W.text '</textarea>'];
        
    case 'select'
        
        if ~isempty(in.W.label)
            out = [out '<label for="' in.W.tag '">' in.W.label '</label>'];
        end
        
        out = [out '<select id="' in.W.tag];
        
        [OA, CA] = opt_actions(in.W);
        if isempty(CA)
             out = [out '" onchange="document.location=update(''' in.W.tag ''', ''selected'', this.value);"'];
        else
            out = [out '" onchange="document.location=upback(''' in.W.tag ''', ''selected'', this.value' CA ');"'];
        end
        out = [out OA];
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out '>'];
        
        % --- Options
        F = fieldnames(in.W.options);
        for i = 1:numel(F)
            out = [out '<option value="' F{i} '"'];
            if strcmp(F{i}, in.W.selected)
                out = [out ' selected']; 
            end
            out = [out '>' in.W.options.(F{i}) '</option>'];
        end
        
        out = [out '</select>'];
        
    case 'checkbox'
            
        out = [out '<input type="checkbox" id="' in.W.tag '"'];
        
        [OA, CA] = opt_actions(in.W);
        if isempty(CA)
            out = [out '" onchange="document.location=update(''' in.W.tag ''', ''selected'', this.checked);"'];
        else
            out = [out '" onchange="document.location=upback(''' in.W.tag ''', ''selected'', this.checked' CA ');"'];
        end
        out = [out OA];
        
        if ~isempty(in.W.style), out = [out ' style="' in.W.style '"']; end
        if in.W.disabled, out = [out ' disabled']; end
        
        out = [out '><label for="' in.W.tag '">' in.W.label '</label>'];
        
end

end

% -------------------------------------------------------------------------
function [OA, CA] = opt_actions(W)

OA = '';
CA = '';
f = fieldnames(W);

for i = 1:numel(f)
    if ML.isfunction_handle(W.(f{i}))
        switch f{i}
            case 'change'
                CA = [', ''' W.tag ''', ''' func2str(W.(f{i})) ''''];
            otherwise
                OA = [OA ' on' f{i} '="document.location=callback(''' W.tag ''', ''' func2str(W.(f{i})) ''');"'];
        end
    end
end

end

function tag = button(this, varargin)
%ML.HUI.button HUI pushbutton
%   ELM = ML.HUI.BUTTON(TEXT) Creates a HUI pushbutton containing TEXT.
%
%   ML.HUI.BUTTON(..., EVENT, CALLBACK) adds a callback related to an 
%   event. Possible values for EVENT are:
%   - 'click'
%   - 'mouseover'
%   - 'mouseout'
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.text = @ischar;
in.tab(this.current_tab) = @isnumeric;
in.tag('') = @ischar;
in.style('') = @ischar;

in.data(struct()) = @isstruct;
in.click('') = @ML.isfunction_handle;
in.mouseover('') = @ML.isfunction_handle;
in.mouseout('') = @ML.isfunction_handle;
in.visible(true) = @islogical;
in.disabled(false) = @islogical;

in = +in;

% =========================================================================

% --- Define widget
in.type = 'button';
in.tag = this.def_widget(in);

% --- Output
if nargout
    tag = in.tag;
end
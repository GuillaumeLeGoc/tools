function out = show_widget(this, varargin)
%ML.GUI.show_widget
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;
in.W = @(x) ischar(x) || isstruct(x);
in.pos = @isnumeric;
in = +in;

% =========================================================================

% --- Get widget
if ischar(in.W)
    in.W = this.get_widget(in.W);
end

% --- Define widget

if strcmp(in.W.params{2}, 'html')
    
    % Define html object
    jObject = com.mathworks.mlwidgets.html.HTMLBrowserPanel;
    [browser, h] = javacomponent(jObject, [], this.fig);
    
    % Settings
    browser.setHtmlText(ML.getarg(in.W.params, 'html'));
    set(h, 'Tag', ML.getarg(in.W.params, 'Tag'));
    set(h, 'Position', in.pos);
    
else
    
    h = uicontrol(this.fig, in.W.params{:});
    
    % --- Specific settings
    switch get(h, 'Style')
        
        case 'text'
            
            % Specified width
            if ~isfinite(in.W.width)
                in.W.width = this.width;
            end
            
            if ~isnan(in.W.width)
                set(h, 'Position', [0 0 in.W.width 1]);
            end
            
            % Recommended width and height
            [~, pos] = textwrap(h, get(h, 'String'));
            set(h, 'Position', pos);
            
        case 'checkbox'
            
            % Recommentded width and height
            [~, pos] = textwrap(h, strrep(get(h,'String'), ' ', '_'));
            pos(3) = pos(3) + 15;
            set(h, 'Position', pos);
            
            
        case 'popupmenu'
            
            if ~isnan(in.W.width)
                pos = get(h, 'Position');
                set(h, 'Position', [pos(1:2) in.W.width pos(4)]);
            end
            
    end
    
    
    
    % --- Positionning
    pos = get(h, 'Position');
    
    switch in.W.halign
        case 'left'
            x = in.pos(1);
            
        case 'center'
            x = in.pos(1) + (in.pos(3) - pos(3))/2;
            
        case 'right'
            x = in.pos(1) + in.pos(3) - pos(3);
    end
    
    switch in.W.valign
        case 'bottom'
            y = in.pos(2);
            
        case 'center'
            y = in.pos(2) + (in.pos(4) - pos(4))/2;
            
        case 'top'
            y = in.pos(2) + in.pos(4) - pos(4);
    end
    
    set(h, 'Position', [x y pos(3:4)]);

end

% --- Output
if nargout
    out = h;
end


function show_tab(this, varargin)
%ML.GUI.show_tab Display the GUI
%
%   See also: 

% === Inputs ==============================================================

in = ML.Input();
in.tab = @isnumeric;
in = +in;

% =========================================================================

this.display_tab = in.tab;
this.show;
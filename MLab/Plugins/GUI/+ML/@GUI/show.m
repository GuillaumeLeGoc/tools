function show(this, varargin)
%ML.GUI.show Display the GUI
%
%   See also:

% === Parameters ==========================================================

margin = 10;
padding = 10;

C_bkg = ML.grey(0.95);
C_ec = ML.grey(0.7);

% =========================================================================

% --- Figures properties --------------------------------------------------

% Get figure by name
Fig = ML.Figures;
this.fig = Fig.select(this.name);
clf(this.fig)
delete(setdiff(findobj(gcf),gcf));

% Misc properties
set(this.fig, 'Num', 'off');
set(this.fig, 'Color', C_bkg);

% Get figure display size
tmp = get(this.fig, 'Position');
this.width = tmp(3);
this.height = tmp(4);

% Re-show on resize
set(this.fig, 'ResizeFcn', @this.show);

% --- Tabs gestion --------------------------------------------------------

if ~numel(this.tabs)==1 || ~isempty(this.tabs(1).name)
    
    % Preparation
    shift = margin;
    
    for i = 1:numel(this.tabs)
        
        % Define textbox
        hT = annotation('textbox', 'Position', [0 0 0 0], 'String', this.tabs(i).name, ...
            'Units', 'pixels', 'BackgroundColor', C_bkg, 'EdgeColor', 'none');
        
        % Manage position
        set(hT, 'FitBoxToText', 'on');
        tmp = get(hT, 'Position');
        set(hT, 'Position', [shift this.height-margin-tmp(4) tmp(3) tmp(4)]);
        
        % Contour
        h1 = annotation('line', [0 0], [0 0], 'Units', 'pixels', 'color', C_ec);
        set(h1, 'Position', [shift this.height-margin-tmp(4)+1 0 tmp(4)]);
        
        h2 = annotation('line', [0 0], [0 0], 'Units', 'pixels', 'color', C_ec);
        set(h2, 'Position', [shift this.height-margin+1 tmp(3) 0]);
        
        if i==numel(this.tabs)
            h3 = annotation('line', [0 0], [0 0], 'Units', 'pixels', 'color', C_ec);
            set(h3, 'Position', [shift+tmp(3) this.height-margin-tmp(4)+1 0 tmp(4)]);
        end
        
        if i==this.display_tab
            H = [hT h1 h2];
            if exist('h3', 'var'), H = [H h3]; end
        else
            
            % Define Callback
            set(hT, 'ButtonDownFcn', @(h,e) this.show_tab(i));
            
        end
        
        % Shift update
        shift = shift + tmp(3);
        
    end
    
    % Background
    hB = annotation('rectangle', 'Units', 'pixels', 'FaceColor', 'w', 'EdgeColor', C_ec);
    Mpos = [margin margin this.width-2*margin+1 this.height-2*margin-tmp(4)+1];
    set(hB, 'Position', Mpos);
    
    % Bring active tab to front
    set(H(1), 'BackgroundColor', 'w');
    for i = 1:numel(H)
        uistack(H(i), 'top');
    end
    
else
    
    % Background
    hB = annotation('rectangle', 'Units', 'pixels', 'FaceColor', 'w', 'EdgeColor', C_ec);
    Mpos = [margin margin this.width-2*margin+1 this.height-2*margin+1];
    set(hB, 'Position', Mpos);
    
end

% --- Main content --------------------------------------------------------

[nrow, ncol] = size(this.tabs(this.display_tab).layout);

% --- Column widths and row heights
W = NaN(1, ncol);
H = NaN(1, nrow);

for i = 1:nrow
    for j = 1:ncol
        
        if ~isempty(this.tabs(this.display_tab).layout(i,j).Width)
            W(j) = nanmax(W(j), this.tabs(this.display_tab).layout(i,j).Width);
        end
        
        if ~isempty(this.tabs(this.display_tab).layout(i,j).Height)
            H(i) = nanmax(H(i), this.tabs(this.display_tab).layout(i,j).Height);
        end
        
    end
end

W(isnan(W)) = (Mpos(3) - nansum([0 W]) - 2*padding)/nnz(isnan(W));
H(isnan(H)) = (Mpos(4) - nansum([0 H]) - 2*padding)/nnz(isnan(H));

% --- Position function handle
getpos = @(i,j) [Mpos(1)+padding+sum(W(1:j-1)) Mpos(4)+Mpos(2)-padding-sum(H(1:i))];

% --- Grid display
if strcmp(this.style, 'grid')
    
    for i = 1:nrow
        for j = 1:ncol
            
            h = annotation('rectangle', 'Units', 'pixels', 'LineStyle', '-', ...
                'EdgeColor', ML.grey(0.8));
            set(h, 'Position', [getpos(i,j) W(j) H(i)]);
            
        end
    end
    
end

% --- Widgets

for i = 1:numel(this.tabs(this.display_tab).widgets)
    
    Wdg = this.tabs(this.display_tab).widgets{i};
    
    % Get bounding box
    bbox = [getpos(Wdg.row, Wdg.col) ...
        sum(W(Wdg.col:Wdg.col+Wdg.colspan-1)) ...
        sum(H(Wdg.row:Wdg.row+Wdg.rowspan-1))];
    bbox(2) = bbox(2) - sum(H(Wdg.row+1:Wdg.row+Wdg.rowspan-1));
    
    this.tabs(this.display_tab).widgets{i}.h = this.show_widget(Wdg, bbox);
    
    
end

function tag = def_widget(this, varargin)
%ML.GUI.def_widget
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;
in.W = @isstruct;
in = +in;

% =========================================================================

% --- Auto tag completion
for i = 1:2:numel(in.W.params)
    if strcmp(in.W.params{i}, 'Tag')
        tagpos = i+1;
        break;
    end
end

if this.auto_tag && isempty(in.W.params{tagpos})
    in.W.params{tagpos} = ML.uniqid;
end

% --- Grid layout management
if this.grid_layout 
    
    % Rowspan shortcut
    if numel(in.W.row)>1
        in.W.rowspan = numel(in.W.row);
        in.W.row = in.W.row(1);
    end
    
    % Colspan shortcut
    if numel(in.W.col)>1
        in.W.colspan = numel(in.W.col);
        in.W.col = in.W.col(1);
    end
end

% --- Define the widget
this.tabs(in.W.tab).widgets{end+1} = in.W;

% --- Layout type peculiarities
if this.grid_layout 
    
    % Extend option grid ?
    if any(size(this.tabs(in.W.tab).layout) < [max(in.W.row) max(in.W.col)])
        this.tabs(in.W.tab).layout(max(in.W.row), max(in.W.col)).Width = [];
        this.tabs(in.W.tab).layout(max(in.W.row), max(in.W.col)).Height = [];
        this.tabs(in.W.tab).layout(max(in.W.row), max(in.W.col)).BackgroundColor = [];
    end
    
end

% --- Output
if nargout
    tag = in.W.params{tagpos};
end
classdef Steps<handle
%ML.Steps is the Steps class for Temporary HTML Files.
    
    % --- PROPERTIES ------------------------------------------------------
    properties (SetAccess = public)
        
    end
    
    properties (SetAccess = protected)
        elms = {};
        steps = {};
        status = {};
        
        tick = '<font style=''color:#04B431''>&#10004;</font>';
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = Steps()
        %ML.Steps constructor
            
        end
                        
    end
end

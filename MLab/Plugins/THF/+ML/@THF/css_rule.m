function css_rule(this, rule)
%[THF].css_rule Add a css rule
%
%   See also .

% --- Update the content
this.css = [this.css rule char(10)];
function out = add(this, text, tag)
%[THF].add Add tagged text.
%
%   See also: ML.THF, ML.THF.replace.

% --- Identifier
uid = ML.uniqid;

% --- Default tag
if ~exist('tag', 'var'), tag = 'span'; end

% --- Specific tags
switch tag
    case 'item'
        text = ['<li>' text '</li>'];
        tag = 'ul';
end

% --- Update the content
this.content = [this.content sprintf('<%s id="%s">%s</%s id="%s">\n', tag, num2str(uid), text, tag, num2str(uid))];

% --- Output
if nargout
    out = uid;
end
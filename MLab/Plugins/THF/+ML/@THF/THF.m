classdef THF<handle
%THF is the Temporary HTML Files class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        
        fname = tempname;
        css = '';
        content = '';
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = THF()
        %THF::constructor
                    
        end
                
        % _________________________________________________________________
        function this = close(this)
        %THF.CLOSE Close the THF browser and the temporary file
        
            mclose('web');
        
        end
       
    end
end

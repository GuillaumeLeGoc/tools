function out = display(this)
%[THF].display Display the temporary html file.
%
%   See also: ML.THF.

% Modify content in the temporary file

s = sprintf('<html>\n<head>\n<style type="text/css">\n%s</style>\n</head>\n<body>\n%s</body></html>', ...
        this.css, strrep(this.content, '\', '\\'));

if nargout   
    out = s;
else
    
    % Write to temporary file
    fid = fopen(this.fname, 'w');
    warning off
    fprintf(fid, s);
    warning on
    fclose(fid);
    
    % Display content
    web(this.fname);
    
    pause(0.2)
    commandwindow;
    
end
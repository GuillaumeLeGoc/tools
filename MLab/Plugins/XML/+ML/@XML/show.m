function show(this, varargin)
%ML.XML.show [XML plugin] Show the XML document
%   ML.XML.SHOW() builds the XML object's tree and displays it in the
%   browser.
%
%   ML.XML.BUILD(XML) shows the XML string in the browwser.
%
%   See also ML.XML, ML.XML.build.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('xml', this.build, @ischar);
in = +in;

% =========================================================================

% Write to temporary file
fid = fopen(this.filename, 'w');
fprintf(fid, in.xml);
fclose(fid);

% Display content
web(this.filename);
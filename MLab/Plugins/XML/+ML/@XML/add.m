function out = add(this, varargin)
%ML.XML.add [XML plugin] Add an element to the XML tree
%   ML.XML.ADD(POS, TYPE, NAME) adds an element to the XML object's tree. 
%   POS can be either a one element struture with two numeric fields 
%   'parent' and 'pos'. POS can also be a number refering to the parent 
%   element, in this case the new element is added at the end of the 
%   children list. TYPE is a string which can be:
%   - 'text':    Simple text, without tag
%   - 'utag':    Unary tag
%   - 'tag':     Inline tag
%   - 'cont':    Block-like tag
%   - 'comment': Comment (<!-- ... -->)
%   - 'cdata':   CDATA section (used to escape blocks of text containing 
%                characters which would otherwise be recognized as markup).
%   - 'pi':      Processing instructions (PIs) allow documents to contain 
%                instructions for applications (<? ... ?>).
%   NAME is the name of the tag.
%
%   ML.XML.ADD(POS, TYPE, CONTENT) specifies the content of the new
%   element. 
%
%   ID = ML.XML.ADD(...) returns the identifier of the newly created tag.
%
%   See also ML.XML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addRequired('pos', @ML.isXMLpos);
in.addRequired('type', @ischar);
in.addRequired('name', @ischar);
in.addParamValue('content', [], @(x) ischar(x) || isnumeric(x));
in.addParamValue('options', '', @(x) isstruct(x) || iscell(x));
in = +in;

% =========================================================================

% Get id
id = numel(this.tags)+1;

% --- Parent and position
if isstruct(in.pos)
    
    this.tags(id).parent = in.pos.parent;
    this.tags(id).pos = in.pos.pos;
    
else
    
    this.tags(id).parent = in.pos;
    if in.pos
        switch this.tags(in.pos).type
            case {'cont', 'tag'}
                this.tags(in.pos).content(end+1) = id;
            otherwise
                warning(['Cannot happend elements to tags of type ' this.tags(in.pos).type]);
                out = NaN;
                return
        end
    end
    
end  
        
% --- Type specific fields
this.tags(id).type = in.type;

switch in.type
    
    case {'cont', 'tag', 'pi'}
        
        this.tags(id).name = in.name;
        this.tags(id).content = in.content;
        this.tags(id).options = in.options;
    
    case 'utag'
        
        this.tags(id).name = in.name;
        this.tags(id).content = '';
        this.tags(id).options = in.options;
        
    case {'text', 'comment', 'cdata'}
        
        this.tags(id).name = '';
        this.tags(id).content = in.name;
        this.tags(id).options = struct();
        
end

% --- Output

if nargout
    out = id;
end
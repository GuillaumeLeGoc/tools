function out = pre(this, varargin)
%ML.HTML.pre Add a <pre> element to the HTML object.
%   ML.HTML.PRE() add a <pre> element to the HTML object.
%
%   ML.HTML.PRE(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.PRE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.PRE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'pre', 'options', notin);

% --- Output
if nargout
    out = id;
end
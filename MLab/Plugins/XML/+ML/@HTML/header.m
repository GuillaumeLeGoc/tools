function out = header(this, varargin)
%ML.HTML.header Add a <header> element to the HTML object.
%   ML.HTML.HEADER() add a <header> element to the HTML object.
%
%   ML.HTML.HEADER(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.HEADER(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.HEADER(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'header', 'options', notin);

% --- Output
if nargout
    out = id;
end
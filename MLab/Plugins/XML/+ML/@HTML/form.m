function out = form(this, varargin)
%ML.HTML.form Add a <form> element to the HTML object.
%   ML.HTML.FORM() add a <form> element to the HTML object.
%
%   ML.HTML.FORM(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.FORM(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.FORM(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'form', 'options', notin);

% --- Output
if nargout
    out = id;
end
function out = select(this, varargin)
%ML.HTML.select Add a <select> element to the HTML object.
%   ML.HTML.SELECT() add a <select> element to the HTML object.
%
%   ML.HTML.SELECT(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.SELECT(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.SELECT(...) returns the identifier of the new element.
%
%   See also ML.HTML, ML.HTML.optgroup.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'select', 'options', notin);

% --- Output
if nargout
    out = id;
end
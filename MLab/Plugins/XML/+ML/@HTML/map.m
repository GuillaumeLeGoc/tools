function out = map(this, varargin)
%ML.HTML.map Add a <map> element to the HTML object.
%   ML.HTML.MAP() add a <map> element to the HTML object.
%
%   ML.HTML.MAP(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.MAP(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.MAP(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'map', 'options', notin);

% --- Output
if nargout
    out = id;
end
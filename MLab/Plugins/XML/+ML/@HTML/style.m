function [out1, out2] = style(this, varargin)
%ML.HTML.style Add a <style> element to the HTML object.
%   ML.HTML.STYLE() add a <style> element to the HTML object.
%
%   ML.HTML.STYLE(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.STYLE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.STYLE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.head varargin];
end

in = ML.Input;
in.position = @ML.isXMLpos;
in.text = @ischar;
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'style', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Output
if nargout
    out1 = id;
    out2 = id_txt;
end
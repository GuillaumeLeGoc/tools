function out = img(this, varargin)
%ML.HTML.img Add an <img> element to the HTML object.
%   ML.HTML.IMG() add an <img> element to the HTML object.
%
%   ML.HTML.IMG(SOURCE) specifies the source of the element.
%
%   ML.HTML.IMG(POSITION, SOURCE) also specifies the position of the 
%   element.
%
%   ML.HTML.IMG(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.IMG(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('src', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['src' ; in.src; notin];
id = this.add(in.position, 'utag', 'img', 'options', notin);

% --- Outputs
if nargout
    out = id;
end
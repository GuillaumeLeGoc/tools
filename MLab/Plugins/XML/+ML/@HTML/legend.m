function [out1, out2] = legend(this, varargin)
%ML.HTML.legend Add a <legend> element to the HTML object.
%   ML.HTML.LEGEND() add a <legend> element to the HTML object.
%
%   ML.HTML.LEGEND(TEXT) specifies the text content of the element.
%
%   ML.HTML.LEGEND(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.LEGEND(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.LEGEND(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'legend', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end
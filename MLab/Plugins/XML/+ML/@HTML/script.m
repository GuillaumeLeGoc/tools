function out = script(this, varargin)
%ML.HTML.script Add a <script> element to the HTML object.
%   ML.HTML.SCRIPT() add a <script> element to the HTML object.
%
%   ML.HTML.SCRIPT(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.SCRIPT(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.SCRIPT(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'script', 'options', notin);

% --- Output
if nargout
    out = id;
end
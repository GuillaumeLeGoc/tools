function out = area(this, varargin)
%ML.HTML.area Add an <area> element to the HTML object.
%   ML.HTML.AREA() add an <area> element to the HTML object.
%
%   ML.HTML.AREA(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.AREA(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.AREA(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'area', 'options', notin);

% --- Outputs
if nargout
    out = id;
end
function out = thead(this, varargin)
%ML.HTML.thead Add a <thead> element to the HTML object.
%   ML.HTML.THEAD() add a <thead> element to the HTML object.
%
%   ML.HTML.THEAD(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.THEAD(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.THEAD(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'thead', 'options', notin);

% --- Output
if nargout
    out = id;
end
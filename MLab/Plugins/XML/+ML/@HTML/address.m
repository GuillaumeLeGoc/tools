function out = address(this, varargin)
%ML.HTML.footer Add a <address> element to the HTML object.
%   ML.HTML.ADDRESS() add a <address> element to the HTML object.
%
%   ML.HTML.ADDRESS(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.ADDRESS(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.ADDRESS(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'address', 'options', notin);

% --- Output
if nargout
    out = id;
end
function [out1, out2] = object(this, varargin)
%ML.HTML.object Add a <object> element to the HTML object.
%   ML.HTML.OBJECT() add a <object> element to the HTML object.
%
%   ML.HTML.OBJECT(TEXT) specifies the text content of the element.
%
%   ML.HTML.OBJECT(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.OBJECT(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.OBJECT(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'object', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end
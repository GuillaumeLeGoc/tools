function out = article(this, varargin)
%ML.HTML.article Add a <article> element to the HTML object.
%   ML.HTML.ARTICLE() add a <article> element to the HTML object.
%
%   ML.HTML.ARTICLE(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.ARTICLE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.ARTICLE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'article', 'options', notin);

% --- Output
if nargout
    out = id;
end
function out = span(this, varargin)
%ML.HTML.span Add a <span> element to the HTML object.
%   ML.HTML.SPAN() add a <span> element to the HTML object.
%
%   ML.HTML.SPAN(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.SPAN(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.SPAN(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'span', 'options', notin);

% --- Output
if nargout
    out = id;
end
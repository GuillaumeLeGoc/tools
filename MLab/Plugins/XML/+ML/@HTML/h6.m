function out = h6(this, varargin)
%ML.HTML.h6 Add a <h6> element to the HTML object.
%   ML.HTML.H6() add a <h6> element to the HTML object.
%
%   ML.HTML.H6(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H6(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H6(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h6', 'options', notin);

% --- Output
if nargout
    out = id;
end
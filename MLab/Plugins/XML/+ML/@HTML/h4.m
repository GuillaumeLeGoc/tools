function out = h4(this, varargin)
%ML.HTML.h4 Add a <h4> element to the HTML object.
%   ML.HTML.H4() add a <h4> element to the HTML object.
%
%   ML.HTML.H4(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H4(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H4(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h4', 'options', notin);

% --- Output
if nargout
    out = id;
end
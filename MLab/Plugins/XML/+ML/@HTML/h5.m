function out = h5(this, varargin)
%ML.HTML.h5 Add a <h5> element to the HTML object.
%   ML.HTML.H5() add a <h5> element to the HTML object.
%
%   ML.HTML.H5(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H5(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H5(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h5', 'options', notin);

% --- Output
if nargout
    out = id;
end
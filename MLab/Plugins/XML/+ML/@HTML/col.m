function out = col(this, varargin)
%ML.HTML.col Add an <col> element to the HTML object.
%   ML.HTML.COL() add an <col> element to the HTML object.
%
%   ML.HTML.COL(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.COL(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.COL(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'col', 'options', notin);

% --- Outputs
if nargout
    out = id;
end
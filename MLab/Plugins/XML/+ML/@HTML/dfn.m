function [out1, out2] = dfn(this, varargin)
%ML.HTML.dfn Add a <dfn> element to the HTML object.
%   ML.HTML.DFN() add a <dfn> element to the HTML object.
%
%   ML.HTML.DFN(TEXT) specifies the text content of the element.
%
%   ML.HTML.DFN(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.DFN(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.DFN(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML, ML.HTML.del.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'dfn', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end
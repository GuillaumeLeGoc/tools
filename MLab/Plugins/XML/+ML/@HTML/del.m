function [out1, out2] = del(this, varargin)
%ML.HTML.del Add a <del> element to the HTML object.
%   ML.HTML.DEL() add a <del> element to the HTML object.
%
%   ML.HTML.DEL(TEXT) specifies the text content of the element.
%
%   ML.HTML.DEL(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.DEL(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.DEL(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML, ML.HTML.ins.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'del', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end
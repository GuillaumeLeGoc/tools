function out = section(this, varargin)
%ML.HTML.section Add a <section> element to the HTML object.
%   ML.HTML.SECTION() add a <section> element to the HTML object.
%
%   ML.HTML.SECTION(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.SECTION(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.SECTION(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'section', 'options', notin);

% --- Output
if nargout
    out = id;
end
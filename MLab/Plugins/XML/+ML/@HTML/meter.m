function [out1, out2] = meter(this, varargin)
%ML.HTML.meter Add a <meter> element to the HTML object.
%   ML.HTML.METER() add a <meter> element to the HTML object.
%
%   ML.HTML.METER(TEXT) specifies the text content of the element.
%
%   ML.HTML.METER(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.METER(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.METER(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'meter', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end
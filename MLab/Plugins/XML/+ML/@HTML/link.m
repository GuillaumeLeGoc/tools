function out = link(this, varargin)
%ML.HTML.link Add an <link> element to the HTML object.
%   ML.HTML.LINK() add an <link> element to the HTML object.
%
%   ML.HTML.LINK(POSITION) specifies the position of the element.
%
%   ML.HTML.LINK(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.LINK(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'link', 'options', notin);

% --- Outputs
if nargout
    out = id;
end
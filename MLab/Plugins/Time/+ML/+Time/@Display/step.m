function step(this, text)
%ML.Timer.step Timer step%
%   ML.TIMER.STEP(STEP_TXT) Marks a step in the Timer 
%
%*  See also ML.Timer.start, ML.Timer.stop.

% --- Checks
if ~isnan(this.t0)
    fprintf(' %.2f sec\n', toc(this.t0));
end

% --- Display step text
if exist('text', 'var')
    fprintf('%s ...', text);
end
    
% --- Start timer
this.t0 = tic;


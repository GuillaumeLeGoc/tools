classdef Display<handle
    %ML.Time.Display Time display class
    %   ML.Time.DIsplay is the time display class.
    
    properties (SetAccess = public)
        t0 = NaN;
        period = 1;
        
    end
    
    properties (SetAccess = private)
        counter = -1;
        itot = Inf;
        range = struct();
        cwl = NaN;
        rmlast = false;
    end
    
    methods
        % _________________________________________________________________
        function this = Display()
            %ML.Time.Display Constructor
            
            % --- Command window length
            try 
                cws = get(0, 'CommandWindowSize');
            catch
                cws = matlab.desktop.commandwindow.size;
            end
            this.cwl = cws(1);
            
        end
    end
    
end

function start(this, varargin)
%ML.Timer.start Start the timer
%   ML.TIMER.START() starts the Timer.
%
%   ML.TIMER.START(STEP_TXT) starts the Timer and display the step text
%   STEP_TXT.
%
%*  See also ML.Timer.step, ML.Timer.stop.

this.step(varargin{:});

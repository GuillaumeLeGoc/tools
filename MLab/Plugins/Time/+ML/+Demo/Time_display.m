clc

cld = ML.Time.Display;

cld.start('First step');
pause(0.1);

% cld.start('Second step');
% pause(0.1);

for i = 1:10
    
    pause(0.1);
    
    cld.waitline(i, 'Test');
    
end

cld.stop
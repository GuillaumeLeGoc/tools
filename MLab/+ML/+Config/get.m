function [config, fname] = get(varargin)
%ML.Config.get Get MLab configuration structure
%   C = ML.CONFIG.GET() returns the MLab configuration structure.
%
%   C = ML.CONFIG.GET(..., 'cfile', CFILE) returns the configuration
%   structure contained in the file tagged with CFILE in the PREFDIR
%   directory.
%
%   C = ML.CONFIG.GET(..., 'quiet', true) suppress the error message if the
%   configuration file does not exist.
%
%   See also ML.config.
%
%   Reference page in Help browser: <a href="matlab:doc ML.Config.get">doc ML.Config.get</a>
%   <a href="matlab:doc ML">MLab documentation</a>

% === Input variables =====================================================

in = inputParser;
in.addParamValue('cfile', 'MLab', @ischar);
in.addParamValue('quiet', true, @islogical);

in.parse(varargin{:});
in = in.Results;

% -------------------------------------------------------------------------

fname = [prefdir filesep in.cfile '.mat'];

% =========================================================================

% --- Get config
if ~exist(fname, 'file')
    ML.Config.default('cfile', in.cfile, 'quiet', in.quiet);
end

tmp = load(fname);
config = tmp.config;
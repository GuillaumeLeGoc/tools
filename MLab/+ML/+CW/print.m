function [txt, n] = print(varargin)
%ML.CW.print Command window print
%   TXT = ML.CW.PRINT(formatSpec, A1,..., An) returns the text as it should
%   be displayed by the <a href="matlab:help fprintf;">fprintf</a> function.
%
%   [~, N] = ML.CW.PRINT(...) returns the number of characters that should
%   be displayed, excluding the formatting html tags.
%
%   Exemple:
%   >> [txt, N] = ML.CW.print('This is a <a href="matlab:rand(1)">test</a>.');
%
%   >> numel(txt)
%   ans = 
%       45
%
%   >> N
%   ans = 
%       15
%
%   See also fprintf
%
%   More on <a href="matlab:ML.doc('ML.CW.print');">ML.doc</a>

% --- Inputs
in = ML.Input;
in.s = @ischar;
in = +in; 

% --- String preparation
in.s = regexprep(in.s, '<b>', '<strong>');
in.s = regexprep(in.s, '</b>', '</strong>');
in.s = regexprep(in.s, '<u>', '<a href="">');
in.s = regexprep(in.s, '</u>', '</a>');

% --- Outputs
txt = sprintf(in.s, varargin{:});
if nargout==2
    n = numel(regexprep(txt, '</?([^>]*)>', ''));    
end

%! ------------------------------------------------------------------------
%! Author: Raphaël Candelier
%! Version: 1.1
%
%! Revisions
%   1.1     (2015/04/02): Created help.
%   1.0     (2015/01/01): Initial version.
%
%! ------------------------------------------------------------------------
%! Doc
%   <title>To do</title>
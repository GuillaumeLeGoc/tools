function h = addStars(groupedvalues, varargin)
% h = ADDSTARS(groupedvalues);
% h = ADDSTARS(_, 'test', 'ks');
% h = ADDSTARS(_, 'pairs', pairs);
%
% Add significance stars to the current figure. It wraps one of the multi
% significance test (multikstest or multiwilcoxon) to get pvalues and use
% the sigstar function from fileexchange to display stars on the plot.
% See https://github.com/raacampbell/sigstar to get the sigstar function.
%
% INPUTS :
% ------
% groupedvalues : n x  1 cell array containing values for the n groups.
% 'test', value : name of the test : 'kstest', 'wilcoxon', 'kruskalwallis'
% 'pairs', value : a m x 1 cell array containing pairs where to plot the 
% stars. If not provided, all pairs are plotted.
%
% RETURNS :
% -------
% h : output of the sigstar function (handles 

% --- Check input
in = inputParser;
in.addRequired('groupedvalues', @iscell);
in.addParameter('test', 'kstest', @(x) ischar(x)||isstring(x));
in.addParameter('pairs', [], @iscell);
in.parse(groupedvalues, varargin{:});

groupedvalues = in.Results.groupedvalues;
test = in.Results.test;
pairs = in.Results.pairs;

% --- Create pairs if not provided
allpairs = num2cell(nchoosek(1:numel(groupedvalues), 2), 2);
if isempty(pairs)
    pairs = allpairs;
elseif size(pairs, 1) == 1
    pairs = pairs';
end

% --- Perform statistical test
pval = multitest(groupedvalues, test);

% Above tests return an array of all pairs, keep only the selected ones.
p = NaN(numel(pairs), 1);
for id = 1:numel(pairs) 
    p(id) = pval(pairs{id}(1), pairs{id}(2));
end

h = tweaksigstar(pairs, p);
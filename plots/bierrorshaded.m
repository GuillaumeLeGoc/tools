function [h, p] = bierrorshaded(x, y, emin, emax, varargin)
% [h, p] = BIERRORSHADED(x, y, emin, emax)
% [h, p] = BIERRORSHADED(___, 'line', lineoptions)
% [h, p] = BIERRORSHADED(___, 'patch', patchoptions)
% [h, p] = BIERRORSHADED(___, 'ax', axobject)
%
% ERRORSHADED plot y versus x with errors e represented as a shaded area.
%
% INPUTS :
% ------
% x : 1D vector, x-axis.
% y : 1D vector, y-axis.
% emin : 1D vector, lower bond for y
% emax : 1D vector, higher bond for y
% 'line', options : structure, options for the line object.
% 'patch', options : structure, options for the line object.
% 'ax', ax : Axes oject, specify where to plot
%
% Returns :
% -------
% h : handle to the line
% p : handle to the shaded area (patch object)

% --- Check input
p = inputParser;
p.addRequired('x', @isnumeric);
p.addRequired('y', @isnumeric);
p.addRequired('emin', @isnumeric);
p.addRequired('emax', @isnumeric);
p.addParameter('line', [], @(x) isempty(x)||isstruct(x));
p.addParameter('patch', [], @(x) isempty(x)||isstruct(x));
p.addParameter('ax', gca, @(x) isa(x, 'matlab.graphics.axis.Axes'));
p.parse(x, y, emin, emax, varargin{:});

x = p.Results.x;
y = p.Results.y;
emin = p.Results.emin;
emax = p.Results.emax;
lineoptions = p.Results.line;
patchoptions = p.Results.patch;
ax = p.Results.ax;

% Check dimension
if size(x, 1) ~= 1
    x = x';
end
if size(y, 1) ~= 1
    y = y';
end
if size(emin, 1) ~= 1
    emin = emin';
end
if size(emax, 1) ~= 1
    emax = emax';
end

% Fill defaults
if isempty(lineoptions)
    lineoptions = struct;
end
if isempty(patchoptions)
    patchoptions = struct;
end

% Defaults for line
defline = struct;
defline.LineWidth = 1;
defline.LineStyle = '-';
defline.Marker = '.';
defline.MarkerFaceColor = 'none';
defline.MarkerEdgeColor = 'auto';
defline.MarkerSize = 15;
optline = addToStructure(defline, lineoptions);

% Defaults for shaded area
defpatch = struct;
defpatch.FaceAlpha = 0.2;
defpatch.EdgeColor = 'none';
optpatch = addToStructure(defpatch, patchoptions);

% --- Create data to be plotted
ybot = emin;
ytop = emax;

% --- Create figure
hold(ax, 'on');

if ~isfield(optline, 'Color')
    defcolor = ax.ColorOrder(ax.ColorOrderIndex, :);
    optline.Color = defcolor;
end

% --- Plot patch & line
p = patch(ax, [x fliplr(x)], [ytop fliplr(ybot)], optline.Color);
p = mergeStructures(optpatch, p);

h = plot(ax, x, y);
h = mergeStructures(optline, h);

end
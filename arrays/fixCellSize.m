function out = fixCellSize(in, direction)
% out = FIXCELLSIZE(in, direction);
%
% Fill elements of cell in with NaN in order to have the same number of
% elements in all elements in the specified direction.
% Stupidly hardcoded so supports only 2D arrays in cells.
%
% INPUTS :
% ------
% in : cell
% dir : direction (optional)
%
% RETURNS :
% -------
% out : cell with same number of elements in each cell

if ~exist('direction', 'var')
    maxsize = max(cellfun(@numel, in));
    out = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), in, 'UniformOutput', false);
elseif direction == 1 
    maxsize = max(cellfun(@(x) size(x, direction), in));
    out = cellfun(@(x) cat(1, x, NaN(maxsize - size(x, direction), size(x, 2))), in, 'UniformOutput', false);
elseif direction == 2
    maxsize = max(cellfun(@(x) size(x, direction), in));
    out = cellfun(@(x) cat(2, x, NaN(maxsize - size(x, direction), size(x, 1))), in, 'UniformOutput', false);
else
    error('Not implemented.');
end

end
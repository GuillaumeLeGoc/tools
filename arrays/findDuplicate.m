function n = findDuplicate(A)
% Returns number of occurences of identical values in A.

[n, bin] = histc(A, unique(A));
multiple = find(n > 1);
index  = find(ismember(bin, multiple));

end
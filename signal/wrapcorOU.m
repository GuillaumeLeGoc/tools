function [acf, lag] = wrapcorOU(nexp, ntime, dt, k, D, mu, x0, lags)
% mv = VAROU(delays, ousignals);
%
% Computes the ACF of columns in ousignals. Generates said signals from OU
% process.
%
% INPUTS :
% ------
% ousignals : ntimes x nexp
% corrtime : time window for ACF
% dt : scalar, time interval between steps in ousignals
%
% OUTPUT :
% ------
% acf : ntimes x 1 vector, with acf
% lag : 

% --- Init.
ncorr = round(lags(end));    % convert time to steps
acf = NaN(ncorr + 1, nexp);

% --- Signals generation
ousignals = generateOU(nexp, ntime, dt, k, D, mu, x0);

% --- Processing
for idF = 1:nexp
    
    vec = ousignals(:, idF);
    vec = vec - mean(vec);  % center
    
    [r, l] = xcorr(vec, ncorr, 'normalized');
    selected_inds = find(l == 0):find(l == 0) + ncorr;
    tacf = r(selected_inds);
    lag = l(selected_inds)';
    
    if numel(tacf) > numel(lags)
        tacf = tacf(end - 1);
    elseif numel(tacf) < numel(lags)
        tacf(end + 1) = tacf(end);
    end
    
    acf(:, idF) = tacf;
end
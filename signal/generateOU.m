function ousignals = generateOU(nexp, ntime, dt, k, D, varargin)
% ousignal = GENERATEOU(nexp, nstep, th, D, mu, x0);
% 
% Generate nexp time series of a Ornstein-Uhlenbeck process over a ntime
% period sampled with dt interval. Parameters are th (bias), D (diffusion
% coefficient) and mu (drift).
%
% INPUTS :
% ------
% nexp : scalar, number of time series to generate.
% ntime : scalar, duration in unit time.
% dt : scalar, sampling step in unit time.
% k : scalar, theta parameter, corresponding to the bias term in OU equation.
% D : scalar, diffusion coeffecient, corresponding to diffusive term in OU equation
% mu (optional) : scalar, drift term (default is 0).
%
% RETURNS :
% -------
% ousignal : ntime x nexp array containing time series in columns.

% --- Check input
p = inputParser;
p.addRequired('nexp', @isscalar);
p.addRequired('ntime', @isscalar);
p.addRequired('dt', @isscalar);
p.addRequired('k', @isscalar);
p.addRequired('D', @isscalar);
p.addOptional('mu', 0, @isscalar);
p.addOptional('x0', 0, @isnumeric);
p.parse(nexp, ntime, dt, k, D, varargin{:});

p = p.Results;
nexp = p.nexp;
ntime = p.ntime;
dt = p.dt;
k = p.k;
D = p.D;
mu = p.mu;
x0 = p.x0;

% --- Prepare arrays
nsteps = round(ntime/dt);
ousignals = NaN(nsteps, nexp);
ousignals(1, :) = x0;   % initial value

% --- Generate signals
for idt = 1:nsteps - 1
    
    dxdiff = sqrt(2*D).*randn(1, nexp).*sqrt(dt); % diffusion term
    dxrest = k.*(mu - ousignals(idt, :)).*dt;     % restore term
    
    ousignals(idt + 1, :) = ousignals(idt, :) + dxdiff + dxrest;
end
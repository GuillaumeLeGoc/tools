function rts = generateRTS(ntimes, framerate, k, varargin)
% rts = GENERATERTS(nstep, k)
%
% Generate a random telegraph signal with ntimes*framerate elements sampled
% at framerate, with a transition rate of k (same for both transition).
% Default values are 0 and 1.
%
% INPUTS :
% ------
% ntimes : total time
% framerate : framerate
% k : transition rate 
% val (optional) : 2 elements vectors, values the signal can take
%
% OUTPUT :
% ------
% rts : random telegraph signal

% --- Check input
p = inputParser;
p.addRequired('ntimes', @isscalar);
p.addRequired('framerate', @isscalar);
p.addRequired('k', @isscalar);
p.addOptional('val', [0, 1], @isnumeric);
p.parse(ntimes, framerate, k, varargin{:});

ntimes = p.Results.ntimes;
framerate = p.Results.framerate;
k = p.Results.k;
val = p.Results.val;
nsteps = round(ntimes*framerate);

% --- Processing

% Generate residence times
restime = 0;
while sum(restime) < ntimes
    restime = random('Exponential', 1/k, [nsteps, 1]);
end

istop = find(cumsum(restime) > ntimes, 1);
restime = restime(1:istop);
resstep = round(restime.*framerate);
cumrestep = cumsum(resstep);

% Generate signal
rts = NaN(nsteps, 1);
firststep = val(randi([1, 2], 1));	% pick start state
timetoa = cumrestep(1:2:end) + 1;   % time steps where it flips to not firststep
timetob = cumrestep(2:2:end) + 1;   % time steps where it flips to firststep
rts(1) = firststep;
rts(min(timetoa, nsteps)) = val(val ~= firststep);
rts(min(timetob, nsteps)) = val(val == firststep);
rts = fillmissing(rts, 'previous');
rts = rts(1:nsteps);

end
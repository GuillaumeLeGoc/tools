function mv = varOU(delays, ousignals, dt)
% mv = VAROU(delays, ousignals, dt);
%
% Computes the mean moving variance of columns in ousignals, with window
% sizes contained in delays.
%
% INPUTS :
% ------
% delays : 1D vector
% ousignals : ntimes x nexp
% dt : scalar, time interval between steps in ousignals
%
% OUTPUT :
% ------
% mv : ntimes x nexp array, with moving variance with different time
% window size

% --- Check input
p = inputParser;
p.addRequired('delays', @isnumeric);
p.addRequired('ousignals', @isnumeric);
p.addRequired('dt', @isscalar);
p.parse(delays, ousignals, dt);
delays = p.Results.delays;
ousignals = p.Results.ousignals;
dt = p.Results.dt;

nexp = size(ousignals, 2);
ndelays = numel(delays);

% --- Init.
mv = NaN(ndelays, nexp);

% --- Processing
for idt = 1:ndelays
    
    % Select delay
    delt = round(delays(idt)./dt);
    
    % Moving time windows
    varout = movvar(ousignals, delt, 0, 1);
    mv(idt, :) = mean(varout, 1);
    
end
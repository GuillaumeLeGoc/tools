function [pxx, f] = psd_def(x, fs)
% Compute power spectral density from definition.

N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
pxx = (1/(fs*N)) * abs(xdft).^2;
pxx(2:end-1) = 2*pxx(2:end-1);
f = 0:fs/length(x):fs/2;
f = f';
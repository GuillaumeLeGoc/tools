function idc = zerocross(v)
% idc = ZEROCROSS(v);
%
% ZEROCROSS(v) returns the nearest indices where the function in v crosses
% 0.
% From Star Strider on matlabcentral.
%
% INPUTS :
% ------
% v : 1D vector
%
% RETURNS :
% -------
% idc : index in v where it crosses 0.

idc = find(v(:).*circshift(v(:), [-1 0]) <= 0);
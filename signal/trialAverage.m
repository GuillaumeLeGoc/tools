function [timeta, ta, ts] = trialAverage(time, signal, onsets, delayafter, delaybefore)
% ta = TRIALAVERAGE(time, signal, onsets, delay);

% Returns the trial average of signal according to stimulus onsets defined
% by onsets, from onsets to delay. time, stim and delay should be expressed in
% the same units.
%
% INPUTS :
% ------
% time : time vector of signal or array of time series on COLUMN
% signal : signal vector
% onsets : stimulus onsets
% delayafter : scalar, time after onset to compute average in units of time
% delaybefore (optional) : scalar, time before onset, default is 0.
%
% RETURNS :
% ------
% timeta : time vector
% ta : signal average after onsets
% ts : std

% --- Check input
sz = size(signal);
if ~(any(sz == numel(time)))
    error('signal argument does not have the good size.');
elseif sz(1) ~= numel(time)
    signal = signal';
end

if ~exist('delaybefore', 'var')
    delaybefore = 0;
end

% --- Definitions
nstim = numel(onsets);
ndata = size(signal, 2);        % number of time series
timesteps = mean(diff(time));
nintrp = numel(delaybefore:timesteps:delayafter);   % number of points for interpolation

% --- Init.
trials = NaN(nintrp, nstim, ndata);

% --- Processing
for ids = 1:nstim
    
    start = onsets(ids, 1) - delaybefore;
    stop = onsets(ids, 1) + delayafter;
    
    timeq = linspace(start, stop, nintrp);
    
    trials(:, ids, :) = interp1(time, signal, timeq);
    
end

timeta = linspace(-delaybefore, delayafter, nintrp);
ta = squeeze(mean(trials, 2));
ts = squeeze(std(trials, [], 2));
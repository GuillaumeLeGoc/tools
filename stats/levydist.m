function [pd, cd] = levydist(x, mu, c)
% [pd, cd] = LEVYDIST(x, gamma, delta);
% Returns a Lévy distribution estimated in x, of scale parameter c and
% location parameter mu.
%
% INPUTS :
% ------
% x : vector, points where to compute the Lévy distribution.
% c : scalar, scale parameter
% mu : scalar, location parameter
%
% RETURNS :
% -------
% pd : Lévy pdf
% cd : Lévy cdf

% --- Check input
in = inputParser;
in.addRequired('x', @isnumeric);
in.addRequired('mu', @isnumeric);
in.addRequired('c', @isnumeric);
in.parse(x, mu, c);

x = in.Results.x;
mu = in.Results.mu;
c = in.Results.c;

% --- Create stable distribution
S = makedist('Stable', ...
    'alpha', 0.5, ...
    'beta', 1, ...
    'gam', c, ...
    'delta', c + mu);

% --- Return PDF and CDF
pd = pdf(S, x);
cd = cdf(S, x);
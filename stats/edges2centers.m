function centers = edges2centers(edges)
% centers = EDGES2CENTERS(edges);
%
% Converts bin edges to bin centers.

centers = edges(1:end-1) + diff(edges)/2;
end
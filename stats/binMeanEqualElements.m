function [mbin, sebin, cbins, sdbin] = binMeanEqualElements(v1, v2, nbins, fillval, fun)
% [binv1, binv2, nelmts] = BINMEANEQUALELEMENTS(v1, v2, nbins)
% [binv1, binv2, nelmts] = BINMEANEQUALELEMENTS(_, fillval)
% [binv1, binv2, nelmts] = BINMEANEQUALELEMENTS(_, fun)
%
% Bin data in v1 in nbins, with same number of elements in each bin.
% Computes the mean, s.d. and s.e.m. of v2 according to those bins.
% Typical use : one wants to plot v2 versus v1, but there's too many point.
% So you might want to bin the data, and look at the mean +/- s.e. of v2 
% within each bin of v1.
%
% INPUTS :
% ------
% v1 : vector, data to make bins from.
% v2 : vector, data to be averaged.
% nbins : number of bins
% fillval (optional) : if no data found in a bin, replace it by fillval, 
% default is 0). Set to [] to use default value.
% fun (optional) : replace use of mean in means with any compatible handle
% to function.
% 
%
% RETURNS :
% -------
% mbin : mean in each bin
% sdbin : standard deviation in each bin
% sebin : standard error of the mean in each bin
% cbins : bins centers, mbin should be plotted against that.

% --- Check input
if ~exist('fillval', 'var')
    fillval = 0;
elseif isempty(fillval)
    fillval = 0;
end
if ~exist('fun', 'var')
    fun = @mean;
end

% --- Processing

% Find bin edges that span all the data, with the same number of elements
edges = quantile(v1, linspace(0, 1, nbins + 1));

% Find to which bin belong data in v1
binnedv1 = discretize(v1, edges);

% Compute mean & s.d. of v2 within bins
mbin = accumarray(binnedv1, v2, [], fun, fillval);
sdbin = accumarray(binnedv1, v2, [], @std, fillval);

% Count elements in each bins and get s.e.m.
nelmtsperbin = histcounts(v1, edges);
sebin = sdbin./sqrt(nelmtsperbin');

% Get bin centers
cbins = edges2centers(edges);
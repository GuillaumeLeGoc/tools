function [coeff, score, latent, tsquared, explained, mu] = bootpca(nboot, x,varargin)
% [coeff, score, latent, tsquared, explained, mu] = bootpca(x,varargin)
%
% Bootsrapped PCA.

% --- Check input
p = inputParser;
p.addRequired('nboot', @isscalar);
p.addRequired('x', @isnumeric);
p.parse(nboot, x);
nboot = p.Results.nboot;
x = p.Results.x;

% --- Initialization
coeff = cell(nboot, 1);
score = cell(nboot, 1);
latent = cell(nboot, 1);
tsquared = cell(nboot, 1);
explained = cell(nboot, 1);
mu = cell(nboot, 1);

% --- Processing
for it = 1:nboot
    
    resample_data = datasample(x, size(x, 1));
    
    [c, s, l, t, e, m] = pca(resample_data, varargin{:});
    
    coeff{it} = c;
    score{it} = s;
    latent{it} = l;
    tsquared{it} = t;
    explained{it} = e;
    mu{it} = m;
end
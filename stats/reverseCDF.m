function value = reverseCDF(xi, cdf, rd)
% value = REVERSECDF(xi, cdf);
% Pick a random value from possible values, xi, based on the cumulative
% distribution function.
%
% INPUTS :
% ------
% xi : possible values.
% cdf : cumulative distribution function of xi values, must be same size as
% xi.
% rd : impose the value of the cdf to inverse (optional).

% RETURNS :
% -------
% value : random value from xi.

if ~exist('rd', 'var')
    a = min(cdf);
    b = max(cdf);
    rd = randitvl(a, b);                    % pick value of CDF
end

[cdf, mask] = unique(cdf);              % get unique values of CDF
xi = xi(mask);                          % get corresponding values

value = interp1(cdf, xi, rd, 'linear', 'extrap');   % get its corresponding value

end
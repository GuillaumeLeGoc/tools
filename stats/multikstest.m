function [p, stars] = multikstest(X, varargin)
% [p, stars] = MULTIKSTEST(X)
%
% MULTIKSTEST wraps signigicance test kstest2 function, computing a pvalue
% for each combination of columns. 
%
% INPUTS :
% ------
% X : array or cell containing samples on columns. NaN are considered as 
% missing values.
% Everything else is passed to the kstest2 function.
%
% RETURNS :
% -------
% p : array containing p values for each combinations of columns.
% stars : string array containing number of corresponding stars.

[p, stars] = multitest(X, 'kstest', varargin{:});
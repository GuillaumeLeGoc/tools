function [pdf, cdf] = expdistrib(x, lambda, n)
% [pdf, cdf] = EXPDISTRIB(x, lambda, n)
%
% Exponential probability distribution. x must be 1D.
% Returns the following pdf : (x^n)*exp(-lambda*x), normalized to 1.
% If n is not provided, it is set to 0 to get the pure exponential
% distribution.

if ~exist('n', 'var')
    n = 0;
end

% Normalization factor to 1
N = factorial(n)./lambda.^(n+1);

% Fill vector with values
pdf = NaN(size(x));
cdf = NaN(size(x));
inf0 = x < 0;
sup0 = x >= 0;
pdf(inf0) = 0;
pdf(sup0) = (1/N).*x(sup0).^n.*exp(-lambda.*x(sup0));
cdf(inf0) = 0;
cdf(sup0) = gammainc(lambda.*x(sup0), n + 1);
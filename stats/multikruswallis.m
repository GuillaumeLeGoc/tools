function [p, stars] = multikruswallis(X, varargin)
% [p, stars] = MULTIKRUSWALLIS(X)
%
% MULTIKRUSWALLIS wraps signigicance test kruskalwallis function, computing
% a pvalue for each combination of columns. 
%
% INPUTS :
% ------
% X : array or cell containing samples on columns. NaN are considered as 
% missing values.
% Everything else is passed to the ranksum function.
%
% RETURNS :
% -------
% p : vector containing p values for each combinations of columns.
% stars : string array containing number of corresponding stars.
% pairs : vector contaning corresponding columns pairs

[p, stars] = multitest(X, 'kruskalwallis', varargin{:});
function [p, stars] = multitest(X, test, varargin)
% [p, stars] = MULTITEST(X, test, ...);
%
% MULTIKTEST wraps specified signigicance test functions, computing a 
% pvalue for each combination of columns.
%
% INPUTS :
% ------
% X : array or cell containing samples on columns. NaN are considered as 
% missing values.
% Everything else is passed to the significance test function.
% test : char, 'kstest', 'wilcoxon', 'kruskallwallis', 'anova'
%
% RETURNS :
% -------
% p : array containing p values for each combinations of columns.
% stars : string array containing number of corresponding stars.

% --- Check input
in = inputParser;
in.addRequired('X', @(x) isnumeric(x)||iscell(x));
in.addRequired('test', @(x) ischar(x)||isstring(x));
in.parse(X, test);

X = in.Results.X;
statest = in.Results.test;

% - Reshape data and fill with nans missing values
if iscell(X)
    maxsize = max(cellfun(@numel, X));
	X = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), X, 'UniformOutput', false);
    ncell = numel(X);
    nelem = numel(X{1});
    sz = size(X{1});
    dim = find(sz > 1);
    X = reshape(cat(dim, X{:}), nelem, ncell);
end

% --- Prepare output
nsamples = size(X, 2);
p = NaN(nsamples);
stars = cell(nsamples);

for i = 1:nsamples
    
    for j = 1:nsamples
        
        switch statest
            case {'kstest', 'ks'}
                [~, p(i, j)] = kstest2(X(:, i), X(:, j), varargin{:});
            case {'wilcoxon', 'ranksum'}
                p(i, j) = ranksum(X(:, i), X(:, j), varargin{:});
            case 'kruskalwallis'
                p(i, j) = kruskalwallis([X(:, i), X(:, j)], [], 'off', varargin{:});
            case 'anova'
                p(i, j) = anova1([X(:, i), X(:, j)], [], 'off', varargin{:});
            otherwise
                error("Test not implemented, choose from 'kstest', 'wilcoxon', 'kruskalwallis', 'anova'");
        end
        
        if p(i, j) > 0.05
            stars{i, j} = "n.s.";
        elseif p(i, j) <= 0.05 && p(i, j) > 0.01
            stars{i, j} = "*";
        elseif p(i, j) <= 0.01 && p(i, j) > 0.001
            stars{i, j} = "**";
        elseif p(i, j) <= 0.001 && p(i, j) > 0.0001
            stars{i, j} = "***";
        elseif p(i, j) <= 0.0001
            stars{i, j} = "****";
        end
           
    end
end

stars = string(stars);
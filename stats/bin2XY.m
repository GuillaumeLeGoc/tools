function Y2D = bin2XY(X1, X2, Y, binsX1, binsX2, fun)
% X2D = BIN2XY(X1, X2, Y, nbinsX1, nbinsX2, fun)
%
% Split data in Y in bins according to X1 and X2 and apply fun.
% Typically, one wants to represent data Y as a function of X1 and X2 on  a
% 2D map. BIN2XY creates bins from X1 and X2 and computes fun of Y for
% elements belonging to each bin.
% 
% INPUTS : 
% ------
% X1 : predictor 1
% X2 : predictor 2
% Y : response
% binsX1 : edges of bins for X1
% binsX2 : edges of bins for X2
% fun (optional) : function to apply to elements in each bins
%
% RETURNS :
% -------
% Y2D : binned Y. On rows, values according to X2; on columns, values
% according to X1. Use imagesc(Y2D) to display Y in the (X1, X2)
% plane.

% --- Check input
if ~exist('fun', 'var')
    fun = @(X) mean(X, 'omitnan');
end

% --- Init.
nbinsX1 = numel(binsX1) - 1;
nbinsX2 = numel(binsX2) - 1;
Y2D = NaN(nbinsX2, nbinsX1);

% --- Processing
for idX1 = 1:nbinsX1
    
    cond1 = X1 >= binsX1(idX1) & X1 < binsX1(idX1 + 1);
    
    for idX2 = 1:nbinsX2
        
        cond2 = X2 >= binsX2(idX2) & X2 < binsX2(idX2 + 1);
        
        Y2D(idX2, idX1) = fun(Y(cond1 & cond2));
        
    end
end
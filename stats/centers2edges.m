function edges = centers2edges(centers)
% edges = CENTERS2EDGES(centers);
%
% Converts bin centers to bin edges

% Get size
sz = size(centers);
ns = find(sz ~= 1);

d = diff(centers)/2;

if ns == 2
    edges = [centers(1) - d(1), centers(1:end-1) + d, centers(end) + d(end)];
elseif ns == 1
    edges = [centers(1) - d(1); centers(1:end-1) + d; centers(end) + d(end)];
end

edges(2:end) = edges(2:end)+eps(edges(2:end));

end
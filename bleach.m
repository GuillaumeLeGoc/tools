function [] = bleach()
% Clears everything to start fresh.

close all;
evalin('base','clear all');
clc;

end
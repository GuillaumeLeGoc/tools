function d = distanceToLine(point, pline1, pline2)
% d = DISTANCETOLINE(point, pline1, pline2)
%
% distanceToLine returns the shortest distance between a given point and a
% segment defined by two points, pline1 and pline2.
%
% INPUTS :
% ------
% point : (x, y, z) coordinates of the point
% pline 1 : (x, y, z) coordinates of one point of the segment
% pline 2 : (x, y, z) coorsinates of one point of the segment
% If z is not provided, it will be set to 0.
%
% RETURNS :
% -------
% d : shortest distance between the point and the line.

% --- Check input
if numel(point) < 3
    point(3) = 0;
end
if numel(pline1) < 3
    pline1(3) = 0;
end
if numel(pline2) < 3
    pline2(3) = 0;
end

segment = pline1 - pline2;
b = point - pline2;
d = norm(cross(segment, b))/norm(segment);